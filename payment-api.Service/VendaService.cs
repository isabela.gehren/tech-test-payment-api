﻿using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using payment_api.domain;
using payment_api.domain.Venda.Dtos;
using payment_api.domain.Venda.Entities;
using payment_api.domain.Venda.Enums;
using payment_api.domain.Venda.Interfaces;

namespace payment_api.Service
{
    public class VendaService(IVendaRepository vendaRepository, IValidator<CreateVendaDto> createVendaValidator, IMapper mapper) : IVendaService
    {
        private readonly IMapper _mapper = mapper;
        private readonly IVendaRepository _vendaRepository = vendaRepository;
        private readonly IValidator<CreateVendaDto> _createVendaValidator = createVendaValidator;
        public async Task<ResponseModel> CreateAsync(CreateVendaDto dto)
        {
            try
            {
                var vresult = await _createVendaValidator.ValidateAsync(dto);
                if (!vresult.IsValid)
                {
                    return new ResponseModel(vresult.ToDictionary(), StatusCodes.Status400BadRequest);
                }
                Venda venda = _mapper.Map<Venda>(dto);
                venda = await _vendaRepository.SaveAsync(venda);
                return new ResponseModel(new CreateVendaResultDto() { Id = venda.Id }, StatusCodes.Status201Created);
            }
            catch (Exception)
            {
                // TO DO: add log 
                return new ResponseModel(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<ResponseModel> GetAsync(string id)
        {
            try
            {
                Venda venda = await _vendaRepository.GetAsync(id);
                if (venda is null)
                    return new ResponseModel("not_found", StatusCodes.Status400BadRequest);
                return new ResponseModel(_mapper.Map<VendaGetResultDto>(venda));
            }
            catch (Exception)
            {
                // TO DO: add log 
                return new ResponseModel(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<ResponseModel> UpdateStatusAsync(string id, VendaStatus vendaStatus)
        {
            try
            {
                Venda venda = await _vendaRepository.GetAsync(id);
                if (venda is null)
                    return new ResponseModel("not_found", StatusCodes.Status400BadRequest);
                VendaStatus novoStatus = GetNovoStatus(venda, vendaStatus);
                if (novoStatus == venda.Status)
                    return new ResponseModel("invalid_status_change", StatusCodes.Status400BadRequest);

                venda.Status = novoStatus;
                await _vendaRepository.UpdateAsync(venda);
                return new ResponseModel();
            }
            catch (Exception)
            {
                // TO DO: add log 
                return new ResponseModel(StatusCodes.Status500InternalServerError);
            }
        }

        private static VendaStatus GetNovoStatus(Venda venda, VendaStatus vendaStatus)
        {
            switch (venda.Status)
            {
                case VendaStatus.AguardandoPagamento:
                    if (new List<VendaStatus>() { VendaStatus.PagamentoAprovado, VendaStatus.Cancelada }.Contains(vendaStatus))
                        return vendaStatus;
                    else return venda.Status;
                    break;
                case VendaStatus.PagamentoAprovado:
                    if (new List<VendaStatus>() { VendaStatus.EnviadoParaTransportadora, VendaStatus.Cancelada }.Contains(vendaStatus))
                        return vendaStatus;
                    else return venda.Status;
                    break;
                case VendaStatus.EnviadoParaTransportadora:
                    if (new List<VendaStatus>() { VendaStatus.Entregue }.Contains(vendaStatus))
                        return vendaStatus;
                    else return venda.Status;
                    break;
                default:
                    return venda.Status;
                    break;
            }
        }
    }
}
