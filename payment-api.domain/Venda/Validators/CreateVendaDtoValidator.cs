﻿using FluentValidation;
using payment_api.domain.Venda.Dtos;
using payment_api.domain.Vendedor.Interfaces;

namespace payment_api.domain.Venda.Validators
{
    public class CreateVendaDtoValidator : AbstractValidator<CreateVendaDto>
    {
        public CreateVendaDtoValidator(IVendedorRepository vendedorRepository)
        {
            RuleFor(x => x.VendedorId)
             .NotEmpty().WithMessage("field_not_null_or_empty")
             .MustAsync(async (dto, item, context) =>
              {
                  return await vendedorRepository.GetAsync(item) is not null;
              }).WithMessage("not_found"); ;
            RuleFor(x => x.VendaItems)
             .NotEmpty().WithMessage("field_not_null_or_empty");
            RuleForEach(x => x.VendaItems)
                .SetValidator(new CreateVendaItemDtoValidator());
        }
    }
}
