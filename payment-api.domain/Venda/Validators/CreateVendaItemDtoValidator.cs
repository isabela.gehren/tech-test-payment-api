﻿using FluentValidation;
using payment_api.domain.Venda.Dtos;

namespace payment_api.domain.Venda.Validators
{
    public class CreateVendaItemDtoValidator : AbstractValidator<CreateVendaItemDto>
    {
        public CreateVendaItemDtoValidator()
        {
            RuleFor(x => x.Quantidade)
             .NotEmpty().WithMessage("field_not_null_or_empty");
            RuleFor(x => x.Sku)
             .NotEmpty().WithMessage("field_not_null_or_empty");
        }
    }
}
