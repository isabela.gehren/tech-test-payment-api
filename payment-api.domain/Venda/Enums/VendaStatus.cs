﻿namespace payment_api.domain.Venda.Enums
{
    public enum VendaStatus
    {
        AguardandoPagamento = 1,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}
