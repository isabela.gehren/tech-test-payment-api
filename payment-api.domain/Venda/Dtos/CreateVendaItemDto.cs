﻿namespace payment_api.domain.Venda.Dtos
{
    public class CreateVendaItemDto
    {
        public string Sku { get; set; }
        public int Quantidade { get; set; }
    }
}
