﻿namespace payment_api.domain.Venda.Dtos
{
    public class CreateVendaDto
    {
        public string VendedorId { get; set; }
        public IList<CreateVendaItemDto> VendaItems { get; set; }
    }
}
