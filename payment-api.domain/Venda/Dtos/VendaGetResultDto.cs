﻿using payment_api.domain.Venda.Enums;

namespace payment_api.domain.Venda.Dtos
{
    public class VendaGetResultDto
    {
        public string Id { get; set; }
        public DateTime DateTime { get; set; }
        public string VendedorId { get; set; }
        public string VendedorNome { get; set; }
        public VendaStatus Status { get; set; }
        public string StatusLabel { get; set; }
        public IList<VendaItemGetResultDto> VendaItems { get; set; }
    }
}
