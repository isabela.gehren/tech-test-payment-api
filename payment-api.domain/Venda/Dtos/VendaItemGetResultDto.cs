﻿namespace payment_api.domain.Venda.Dtos
{
    public class VendaItemGetResultDto
    {
        public string Id { get; set; }
        public string Sku { get; set; }
        public int Quantidade { get; set; }
    }
}
