﻿using payment_api.domain.Venda.Enums;

namespace payment_api.domain.Venda.Entities
{
    public class Venda
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public DateTime DateTime { get; set; } = DateTime.Now;
        public VendaStatus Status { get; set; } = VendaStatus.AguardandoPagamento;
        public string VendedorId { get; set; }
        public Vendedor.Entities.Vendedor Vendedor { get; set; }
        public IList<VendaItem> VendaItems { get; set; }
    }
}
