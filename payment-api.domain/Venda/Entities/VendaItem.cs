﻿namespace payment_api.domain.Venda.Entities
{
    public class VendaItem
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Sku { get; set; }
        public int Quantidade { get; set; }
    }
}
