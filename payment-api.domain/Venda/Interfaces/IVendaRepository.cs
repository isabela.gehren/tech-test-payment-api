﻿
namespace payment_api.domain.Venda.Interfaces
{
    public interface IVendaRepository
    {
        Task<Venda.Entities.Venda> GetAsync(string id);
        Task<domain.Venda.Entities.Venda> SaveAsync(domain.Venda.Entities.Venda venda);
        Task<domain.Venda.Entities.Venda> UpdateAsync(domain.Venda.Entities.Venda venda);
    }
}
