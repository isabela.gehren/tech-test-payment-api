﻿using payment_api.domain.Venda.Dtos;
using payment_api.domain.Venda.Enums;

namespace payment_api.domain.Venda.Interfaces
{
    public interface IVendaService
    {
        Task<ResponseModel> GetAsync(string id);
        Task<ResponseModel> UpdateStatusAsync(string id, VendaStatus vendaStatus);
        Task<ResponseModel> CreateAsync(CreateVendaDto dto);
    }
}
