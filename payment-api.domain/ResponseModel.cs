﻿namespace payment_api.domain
{
    public class ResponseModel
    {
        public object Content { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }
        public ResponseModel(int statusCode = 200)
        {
            this.StatusCode = statusCode;
        }
        public ResponseModel(string message, int statusCode = 200)
        {
            this.Message = message;
            this.StatusCode = statusCode;
        }
        public ResponseModel(object content, int statusCode = 200)
        {
            this.Content = content;
            this.StatusCode = statusCode;
        }      
    }
}