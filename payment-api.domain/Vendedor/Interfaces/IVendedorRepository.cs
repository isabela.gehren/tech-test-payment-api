﻿namespace payment_api.domain.Vendedor.Interfaces
{
    public interface IVendedorRepository
    {
        Task<Vendedor.Entities.Vendedor> GetAsync(string id);
    }
}
