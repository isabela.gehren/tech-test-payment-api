﻿namespace payment_api.domain.Vendedor.Entities
{
    public class Vendedor
    {
        public string Id { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public IList<Venda.Entities.Venda> Vendas {  get; set; }
    }
}
