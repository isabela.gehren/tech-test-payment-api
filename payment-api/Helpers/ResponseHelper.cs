﻿using Microsoft.AspNetCore.Mvc;
using payment_api.domain;

namespace payment_api.Helpers
{
    public class ResponseHelper : ControllerBase
    {
        public IActionResult CreateResponse(ResponseModel response)
        {
            switch (response.StatusCode)
            {
                case 200:
                    return Ok(response);
                case 201:
                    return StatusCode(201, response);
                case 204:
                    return StatusCode(204);
                case 500:
                    return StatusCode(500, response);
                case 422:
                    return UnprocessableEntity(response);
                case 409:
                    return Conflict(response);
                case 401:
                    return Unauthorized(response);
                case 403:
                    /*
                     * if you don't know why this response is non-standard, don't mess with it :)
                     * https://stackoverflow.com/questions/45095853/how-to-return-403-forbidden-response-as-iactionresult-in-asp-net-core/47708867#47708867
                     */
                    return StatusCode(403, response);
                case 404:
                    return NotFound(response);
                case 400:
                    return BadRequest(response);
                default:
                    return null;
            }
        }
    }
}
