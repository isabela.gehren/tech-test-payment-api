using AutoMapper;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using payment_api.data;
using payment_api.data.Venda;
using payment_api.data.Vendedor;
using payment_api.domain.Venda.Dtos;
using payment_api.domain.Venda.Entities;
using payment_api.domain.Venda.Interfaces;
using payment_api.domain.Venda.Validators;
using payment_api.domain.Vendedor.Entities;
using payment_api.domain.Vendedor.Interfaces;
using payment_api.Service;
using payment_api.Utils.Profiles;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddSingleton(st =>
{
    var mappingConfig = new MapperConfiguration(mc =>
    {
        mc.AddProfile(new VendaProfile());
    });
    return mappingConfig.CreateMapper();
});

builder.Services.AddScoped<IVendaService, VendaService>();

builder.Services.AddScoped<IVendaRepository, VendaRepository>();
builder.Services.AddScoped<IVendedorRepository, VendedorRepository>();

builder.Services.AddScoped<IValidator<CreateVendaDto>, CreateVendaDtoValidator>();


builder.Services.AddDbContext<AppDbContext>();
using (var db = new AppDbContext())
{
    // Adicionar dados de exemplo
    db.Vendedores.Add(new Vendedor { Id = "b5264e94-c6d3-4f86-a464-92d71c328eea", Nome = "Vendedor 1", Email = "vendedor1@mail.com", Cpf = "41572013028", Telefone = "21988556565" });
    db.Vendedores.Add(new Vendedor { Id = "149ff4ea-74dd-4add-a5f5-df17629896d7", Nome = "Vendedor 2", Email = "vendedor2@mail.com", Cpf = "63992093077", Telefone = "21978554545" });
    db.Vendedores.Add(new Vendedor { Id = "f43d1c9c-94cb-45bc-a43a-a0b5af23a3ef", Nome = "Vendedor 3", Email = "vendedor3@mail.com", Cpf = "82193098069", Telefone = "21985852136" });

    db.Vendas.Add(new payment_api.domain.Venda.Entities.Venda()
    {
        Id = "8e6d473a-4030-4286-9517-8bf65831b8cc",
        DateTime = DateTime.Now.AddHours(5).AddDays(-11),
        Status = payment_api.domain.Venda.Enums.VendaStatus.EnviadoParaTransportadora,
        VendedorId = "b5264e94-c6d3-4f86-a464-92d71c328eea",
        VendaItems =
        [
            new VendaItem()
            {
               Id = "d05681f1-4dd0-4eb3-a01e-1a55e3f0d360",
               Sku = "TGR75",
               Quantidade = 2
            }
        ]
    });
    db.SaveChanges();
}
builder.Services.AddRouting(options => options.LowercaseUrls = true);
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(sw =>
{
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    sw.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
