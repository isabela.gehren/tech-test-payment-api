﻿using Microsoft.AspNetCore.Mvc;
using payment_api.domain.Venda.Dtos;
using payment_api.domain.Venda.Enums;
using payment_api.domain.Venda.Interfaces;
using payment_api.Helpers;

namespace payment_api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class VendaController(IVendaService vendaService) : ControllerBase
    {
        private readonly IVendaService _vendaService = vendaService;
        /// <summary>
        /// Busca uma venda
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A venda com o id informado</returns>
        [HttpGet("id")]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(VendaGetResultDto), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetVenda(string id)
        {
            return new ResponseHelper().CreateResponse(await _vendaService.GetAsync(id));
        }

        /// <summary>
        /// Atualiza o status de uma venda
        /// </summary>
        /// <remarks>
        /// Status:
        /// 1 = AguardandoPagamento,
        /// 2 = PagamentoAprovado,
        /// 3 = EnviadoParaTransportadora,
        /// 4 = Entregue,
        /// 5 = Cancelada
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPut("{id}/status/{status}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateStatus(string id, VendaStatus status)
        {
            return new ResponseHelper().CreateResponse(await _vendaService.UpdateStatusAsync(id, status));
        }

        /// <summary>
        /// Cria uma nova venda
        /// </summary>
        /// <remarks>
        /// Vendedores cadastrados no sistema:
        ///  { Id = "b5264e94-c6d3-4f86-a464-92d71c328eea", Nome = "Vendedor 1" }
        ///  { Id = "149ff4ea-74dd-4add-a5f5-df17629896d7", Nome = "Vendedor 2" }
        ///  { Id = "f43d1c9c-94cb-45bc-a43a-a0b5af23a3ef", Nome = "Vendedor 3" }
        /// </remarks>
        /// <param name="dto"></param>
        /// <returns>Id Venda</returns>
        [HttpPost]
        [ProducesResponseType(typeof(IList<string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status201Created)]
        public async Task<IActionResult> CreateVenda(CreateVendaDto dto)
        {
            return new ResponseHelper().CreateResponse(await _vendaService.CreateAsync(dto));
        }
    }
}
