﻿using Microsoft.AspNetCore.Mvc;

namespace payment_api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HealthController : ControllerBase
    {
        /// <summary>
        /// Healthcheck 
        /// </summary>
        /// <returns>OK, if online</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult CheckApi()
        {
            return Ok();
        }
    }
}
