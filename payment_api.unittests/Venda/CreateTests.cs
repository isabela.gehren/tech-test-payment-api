using AutoMapper;
using Castle.Components.DictionaryAdapter;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Moq;
using payment_api.domain;
using payment_api.domain.Venda.Dtos;
using payment_api.domain.Venda.Entities;
using payment_api.domain.Venda.Interfaces;
using payment_api.Service;
using payment_api.Utils.Profiles;

namespace payment_api.unittests.Venda
{
    public class CreateTests
    {
        [Fact]
        public async void DeveRetornar400()
        {
            Mock<IVendaRepository> vendaRepositoryMock = new Mock<IVendaRepository>();
            Mock<IMapper> mapperMock = new Mock<IMapper>();
            Mock<IValidator<CreateVendaDto>> createValidatorMock = new Mock<IValidator<CreateVendaDto>>();
            createValidatorMock.Setup(i => i.ValidateAsync(It.IsAny<CreateVendaDto>(), CancellationToken.None)).Returns(Task.FromResult(new ValidationResult()
            {
                Errors = new List<ValidationFailure>(){
                    new ValidationFailure("prop1", "erro")
                }
            }));
            CreateVendaDto dto = new CreateVendaDto()
            {
                VendedorId = Guid.NewGuid().ToString(),
                VendaItems = new List<CreateVendaItemDto>()
            };

            VendaService service = new VendaService(vendaRepositoryMock.Object, createValidatorMock.Object, mapperMock.Object);
            ResponseModel response = await service.CreateAsync(dto);

            Assert.Equal(StatusCodes.Status400BadRequest, response.StatusCode);
            Assert.True(response.Content is IDictionary<string, string[]>);
        }

        [Fact]
        public async void DeveCriarVenda()
        {
            string vendaId = Guid.NewGuid().ToString();
            string vendedorId = Guid.NewGuid().ToString();
            CreateVendaDto vendaDto = new()
            {
                VendedorId = vendedorId,
                VendaItems = [
                        new CreateVendaItemDto(){
                            Quantidade = 1,
                            Sku = "xpto123"
                        },
                        new CreateVendaItemDto(){
                            Quantidade = 4,
                            Sku = "xpto124"
                        }
                    ]
            };
            Mock<IVendaRepository> vendaRepositoryMock = new Mock<IVendaRepository>();
            vendaRepositoryMock.Setup(i => i.SaveAsync(It.Is<domain.Venda.Entities.Venda>(i => i.Status == domain.Venda.Enums.VendaStatus.AguardandoPagamento && i.DateTime.Date == DateTime.Today)))
                .Returns(Task.FromResult(new domain.Venda.Entities.Venda() { Id = Guid.NewGuid().ToString() })).Verifiable();
            Mock<IValidator<CreateVendaDto>> createValidatorMock = new Mock<IValidator<CreateVendaDto>>();
            createValidatorMock.Setup(i => i.ValidateAsync(It.IsAny<CreateVendaDto>(), CancellationToken.None)).Returns(Task.FromResult(new ValidationResult()));
            IMapper mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new VendaProfile());
            }).CreateMapper();
            VendaService service = new VendaService(vendaRepositoryMock.Object, createValidatorMock.Object, mapper);
            ResponseModel response = await service.CreateAsync(vendaDto);

            vendaRepositoryMock.VerifyAll();
            Assert.Equal(StatusCodes.Status201Created, response.StatusCode);
            Assert.Null(response.Message);
            Assert.True(response.Content is CreateVendaResultDto);
            Assert.NotNull(((CreateVendaResultDto)response.Content).Id);
        }
    }
}