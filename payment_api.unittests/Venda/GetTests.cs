using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Moq;
using payment_api.domain;
using payment_api.domain.Venda.Dtos;
using payment_api.domain.Venda.Entities;
using payment_api.domain.Venda.Interfaces;
using payment_api.Service;
using payment_api.Utils.Profiles;

namespace payment_api.unittests.Venda
{
    public class GetTests
    {
        [Fact]
        public async void DeveRetornar400()
        {
            Mock<IVendaRepository> vendaRepositoryMock = new Mock<IVendaRepository>();
            Mock<IMapper> mapperMock = new Mock<IMapper>();
            Mock<IValidator<CreateVendaDto>> createValidatorMock = new Mock<IValidator<CreateVendaDto>>();

            VendaService service = new VendaService(vendaRepositoryMock.Object, createValidatorMock.Object, mapperMock.Object);
            ResponseModel response = await service.GetAsync(Guid.NewGuid().ToString());

            Assert.Equal(StatusCodes.Status400BadRequest, response.StatusCode);
            Assert.Equal("not_found", response.Message);
            Assert.Null(response.Content);
        }

        [Fact]
        public async void DeveRetornarVenda()
        {
            string vendaId = Guid.NewGuid().ToString();
            string vendedorId = Guid.NewGuid().ToString();
            domain.Venda.Entities.Venda venda = new domain.Venda.Entities.Venda()
            {
                Id = vendaId,
                DateTime = DateTime.Now.AddYears(-1).AddDays(1),
                VendedorId = vendedorId,
                Vendedor = new domain.Vendedor.Entities.Vendedor() { Id = vendedorId, Nome = "Vendedor Teste" },
                Status = domain.Venda.Enums.VendaStatus.Entregue,
                VendaItems = [
                        new VendaItem(){
                            Id = Guid.NewGuid().ToString(),
                            Quantidade = 1,
                            Sku = "xpto123"
                        }
                    ]
            };
            Mock<IVendaRepository> vendaRepositoryMock = new Mock<IVendaRepository>();
            vendaRepositoryMock.Setup(i => i.GetAsync(vendaId)).ReturnsAsync(venda).Verifiable();
            Mock<IValidator<CreateVendaDto>> createValidatorMock = new Mock<IValidator<CreateVendaDto>>();
            IMapper mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new VendaProfile());
            }).CreateMapper();
            VendaService service = new VendaService(vendaRepositoryMock.Object, createValidatorMock.Object, mapper);
            ResponseModel response = await service.GetAsync(vendaId);

            vendaRepositoryMock.VerifyAll();
            Assert.Equal(StatusCodes.Status200OK, response.StatusCode);
            Assert.Null(response.Message);
            Assert.True(response.Content is VendaGetResultDto);
            Assert.NotNull(((VendaGetResultDto)response.Content).VendedorNome);
            Assert.NotNull(((VendaGetResultDto)response.Content).StatusLabel);
            Assert.Single(((VendaGetResultDto)response.Content).VendaItems);
        }
    }
}