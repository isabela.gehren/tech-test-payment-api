﻿using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Moq;
using payment_api.domain.Venda.Dtos;
using payment_api.domain;
using payment_api.domain.Venda.Enums;
using payment_api.domain.Venda.Interfaces;
using payment_api.Service;

namespace payment_api.unittests.Venda
{
    public class UpdateStatusTests
    {
        [Theory]
        [InlineData(VendaStatus.AguardandoPagamento, VendaStatus.EnviadoParaTransportadora)]
        [InlineData(VendaStatus.AguardandoPagamento, VendaStatus.AguardandoPagamento)]
        [InlineData(VendaStatus.AguardandoPagamento, VendaStatus.Entregue)]
        [InlineData(VendaStatus.PagamentoAprovado, VendaStatus.AguardandoPagamento)]
        [InlineData(VendaStatus.PagamentoAprovado, VendaStatus.Entregue)]
        [InlineData(VendaStatus.EnviadoParaTransportadora, VendaStatus.Cancelada)]
        public async Task DeveRetornar400(VendaStatus statusAtual, VendaStatus novoStatus)
        {
            string vendaId = Guid.NewGuid().ToString();
            domain.Venda.Entities.Venda venda = new()
            {
                Id = vendaId,
                Status = statusAtual
            };
            Mock<IVendaRepository> vendaRepositoryMock = new Mock<IVendaRepository>();
            vendaRepositoryMock.Setup(i => i.GetAsync(vendaId)).Returns(Task.FromResult(venda)).Verifiable();
            Mock<IMapper> mapperMock = new Mock<IMapper>();
            Mock<IValidator<CreateVendaDto>> createValidatorMock = new Mock<IValidator<CreateVendaDto>>();

            VendaService service = new VendaService(vendaRepositoryMock.Object, createValidatorMock.Object, mapperMock.Object);
            ResponseModel response = await service.UpdateStatusAsync(vendaId, novoStatus);

            vendaRepositoryMock.VerifyAll();
            Assert.Equal(StatusCodes.Status400BadRequest, response.StatusCode);
            Assert.Equal("invalid_status_change", response.Message);
            Assert.Null(response.Content);
        }

        [Theory]
        [InlineData(VendaStatus.AguardandoPagamento, VendaStatus.PagamentoAprovado)]
        [InlineData(VendaStatus.AguardandoPagamento, VendaStatus.Cancelada)]
        [InlineData(VendaStatus.EnviadoParaTransportadora, VendaStatus.Entregue)]
        public async Task DeveAtualizar(VendaStatus statusAtual, VendaStatus novoStatus)
        {
            string vendaId = Guid.NewGuid().ToString();
            domain.Venda.Entities.Venda venda = new()
            {
                Id = vendaId,
                Status = statusAtual
            };
            Mock<IVendaRepository> vendaRepositoryMock = new Mock<IVendaRepository>();
            vendaRepositoryMock.Setup(i => i.GetAsync(vendaId)).Returns(Task.FromResult(venda)).Verifiable();
            vendaRepositoryMock.Setup(i => i.UpdateAsync(It.Is<domain.Venda.Entities.Venda>(i => i.Id == vendaId && i.Status == novoStatus))).Verifiable();

            Mock<IMapper> mapperMock = new Mock<IMapper>();
            Mock<IValidator<CreateVendaDto>> createValidatorMock = new Mock<IValidator<CreateVendaDto>>();

            VendaService service = new VendaService(vendaRepositoryMock.Object, createValidatorMock.Object, mapperMock.Object);
            ResponseModel response = await service.UpdateStatusAsync(vendaId, novoStatus);

            vendaRepositoryMock.VerifyAll();
            Assert.Equal(StatusCodes.Status200OK, response.StatusCode);
        }
    }
}
