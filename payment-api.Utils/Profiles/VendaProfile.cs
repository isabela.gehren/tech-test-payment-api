﻿using AutoMapper;
using payment_api.domain.Venda.Dtos;
using payment_api.domain.Venda.Entities;

namespace payment_api.Utils.Profiles
{
    public class VendaProfile : Profile
    {
        public VendaProfile()
        {
            CreateMap<Venda, VendaGetResultDto>()
                    .ForMember(x => x.StatusLabel, opt => opt.MapFrom(src => src.Status.ToString()))
                    .ForMember(x => x.VendedorNome, opt => opt.MapFrom(src => src.Vendedor != null ? src.Vendedor.Nome : null))

                ;

            CreateMap<CreateVendaDto, Venda>()
            ;

            CreateMap<VendaItem, VendaItemGetResultDto>()
            ;

            CreateMap<CreateVendaItemDto, VendaItem>()
            ;
        }
    }
}
