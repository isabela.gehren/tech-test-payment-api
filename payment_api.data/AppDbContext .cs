﻿using Microsoft.EntityFrameworkCore;
using payment_api.domain.Venda.Entities;

namespace payment_api.data
{
    public class AppDbContext : DbContext
    {
        public DbSet<domain.Venda.Entities.Venda> Vendas { get; set; }
        public DbSet<VendaItem> VendaItems { get; set; }
        public DbSet<domain.Vendedor.Entities.Vendedor> Vendedores { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "InMemoryDb");
        }
    }
}
