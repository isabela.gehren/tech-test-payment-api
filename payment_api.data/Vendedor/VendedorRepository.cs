﻿using Microsoft.EntityFrameworkCore;
using payment_api.domain.Vendedor.Interfaces;

namespace payment_api.data.Vendedor
{
    public class VendedorRepository(AppDbContext appDbContext) : IVendedorRepository
    {
        private readonly DbSet<domain.Vendedor.Entities.Vendedor> _dbset = appDbContext.Set<domain.Vendedor.Entities.Vendedor>();
        protected readonly AppDbContext _appDbContext = appDbContext;
        public async Task<domain.Vendedor.Entities.Vendedor> GetAsync(string id)
        {
            return await _dbset.Where(i => i.Id == id).FirstOrDefaultAsync();
        }
    }
}
