﻿using Microsoft.EntityFrameworkCore;
using payment_api.domain.Venda.Interfaces;

namespace payment_api.data.Venda
{
    public class VendaRepository(AppDbContext appDbContext) : IVendaRepository
    {
        private readonly DbSet<domain.Venda.Entities.Venda> _dbset = appDbContext.Set<domain.Venda.Entities.Venda>();
        protected readonly AppDbContext _appDbContext = appDbContext;

        public async Task<domain.Venda.Entities.Venda> GetAsync(string id)
        {
            return await _dbset.Include(i => i.VendaItems).Include(i => i.Vendedor).Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<domain.Venda.Entities.Venda> SaveAsync(domain.Venda.Entities.Venda venda)
        {
            await _dbset.AddAsync(venda);
            await _appDbContext.SaveChangesAsync();
            return venda;
        }

        public async Task<domain.Venda.Entities.Venda> UpdateAsync(domain.Venda.Entities.Venda venda)
        {
            _dbset.Update(venda);
            await _appDbContext.SaveChangesAsync();
            return venda;
        }
    }
}
